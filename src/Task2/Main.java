package Task2;
// импорт библиотеки java util Scanner которая используеться для реализации считывания вводимых данных
import java.util.Scanner;

/**
 * Created by user on 02.07.2018.
 */
public class Main {
    //   public static void main(String[] args) {
//
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter number of blocks:");
//        int n = sc.nextInt();
//        sc.close();
//
//        move(n, "slot_a", "slot_b", "slot_c");
//
//    }
//
//    public static void move(int n, String startTruck, String backupPlace, String endTruck) {
//        if (n == 1) {
//            System.out.println("#" + n + " " + startTruck + " -> " + endTruck);
//        } else {
//            move(n - 1, startTruck, endTruck, backupPlace);
//            System.out.println("#" + n + " " + startTruck + " -> " + endTruck);
//            move(n - 1, backupPlace, startTruck, endTruck);
//        }
//    }


        // обьявление метода move который на вход принимает 4 переменных - номер плиты, название автомобиля с которого надо переместить, название промежуточного места, название пустого автомобиля куда надо перенести плиты
        public void move(int n, String startTruck, String backupPlace, String endTruck) {
            // проверка равно ли количество плит единице
            if (n == 1) {
                // вывод инструкции последнего этапа переноса плиты - из стартовой машины на конечную машину
                System.out.println("#" + n + " " + startTruck + " -> " + endTruck);
                // иначе
            } else {
                //вызвать метод move рекурсивно для количиства плит меньшего на 1, и с учетом того что плиту сначала надо будет поставить на промежуточное место
                move(n - 1, startTruck, endTruck, backupPlace);
                // вывести инструкцию для этого этапа
                System.out.println("#" + n + " " + startTruck + " -> " + endTruck);
                // вызвать метод move рекурсивно для количества плит меньшего на 1, и с учетом что плиту из промежуточного места надо будет поставить на целевое место
                move(n - 1, backupPlace, startTruck, endTruck);
            }
        }
        // обьявление метода main в котором производиться ввод данных.
    public static void main(String[] args) {
//            // создание нового екземпляра класа Main
        Main exempl = new Main();
        int blocks;


        // создание нового екзампляра класа сканер
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of blocks: ");
        // ввод количества переносимых плит с использованием проверки, в случае если вводимые данные не являються целым числом вывод сообщения и прекращение роботы програмы
        try{
            //ввод количества переносимых плит
            blocks = scanner.nextInt();}
        catch(Exception e){
            // вывод сообщения
            System.out.println("Input data is not correct");
            //прекращение роботы програмы
            return;
        }
        // вызов рекурсивного метода move для вывода инструкций по переносу плит
        exempl.move(blocks, "slot_a", "slot_b", "slot_c");
    }
}
