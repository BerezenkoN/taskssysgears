package Task3;
///* Моя кошка замечательно разбирается в программировании. Стоит мне объяснить проблему ей - и все становится ясно. */
//John Robbins, Debugging Applications, Microsoft Press, 2000
import java.util.Scanner;

/**
 * Created by user on 02.07.2018.
 */
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter start chanse pls:");
        double inputChanse = sc.nextDouble();
        System.out.println("Enter your order (1 or 2):");
        int order = sc.nextInt();
        sc.close();

        double chanseFirst = inputChanse;
        double chanseSecond = inputChanse;
//10 шагов - 20ходов. Увеличивается линейно, значит каждый шаг на 1/20 от разницы между входящей вероятностью и 1.
        double stepPrice = (1 - inputChanse) / 20;
        int step;
        int stepFirstPlayer = 0;
        int stepSecondPlayer = 0;
        for(step = 1; step <= 20; step++) {
//                деление на 2 по модулю определяет парность числа, для определения кто из игроков ходит
            if(step % 2 == 1) {
                stepFirstPlayer++;
            } else {
                stepSecondPlayer++;
            }
            chanseFirst += stepPrice;
            chanseSecond += stepPrice;
            if(order == 1 && chanseFirst > 0.5 - stepPrice/2) {
                System.out.println("You should fire at " + stepFirstPlayer + "-th step.");
                break;
            }
            if(order == 2 && chanseSecond > 0.5 - stepPrice/2) {
                System.out.println("You should fire at " + stepSecondPlayer + "-th step.");
                break;
            }
        }
    }
}
